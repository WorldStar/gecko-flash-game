﻿package code
{
	/*****************************************
	 * Game2 (Amoebas) :
	 * Demonstrates a spaceship vs amoebas game
	 * involving collision and shooting. This class
	 * wraps the player layer where most of the game
	 * functionality lies. The user interface, game
	 * controls, and elements that need to appear
	 * above the players go here...
	 * -------------------
	 * See 2_amoebas.fla
	 ****************************************/
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	
	public class Game2 extends MovieClip
	{
		//*************************
		// Properties:
		
		public var bonusLifeScore:Number = 2000;
		public var score:Number = 0;
		public var lives:Number = 3;
		public var level:Number = 1;
		
		
		// Flags
		public var buttonFadeOut:Boolean = false;
		
		//*************************
		// Constructor:
		
		public function Game2()
		{
			// Initialize player level
			players.radius2 = circle.width/2;
			players.centerx = circle.x;
			players.centery = circle.y;
			players.live(this);
			initiate();
			// Respond to button events
			start_btn.addEventListener(MouseEvent.CLICK,clickHandler);

			// Update screen every frame
			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}
		
		//*************************
		// Event Handling:
		
		protected function clickHandler( event:MouseEvent ):void
		{
			//var wall_obj:wall_back = new wall_back();
//			switch( event.target )
//			{
//				case start_btn:
//					wall_obj.setflowflg(true);
//					initiate();
//					break;
//			}
			initiate();
		}
		
		protected function enterFrameHandler(event:Event):void
		{
			// Add bonus life every 2000 points
			if( score > bonusLifeScore ){
				bonusLifeScore += 1000;
				lives += 1;
			}
			if(lives > 3){
				lives = 3;
				
			}
			// Update text
			score_txt.text = String(score);
			level_txt.text = String(level);
			
			if(lives ==3)
			{
				mc_livemark1.visible = true;
				mc_livemark2.visible = true;
				mc_livemark3.visible = true;
				
			}
			
			if(lives == 2)
			{
				lives_amrk_init();
				mc_livemark2.visible = true;
				mc_livemark3.visible = true;
			}
			
			if(lives ==1)
			{
				lives_amrk_init();
				mc_livemark3.visible = true;
			}
			
			if(lives ==0)
			{
				lives_amrk_init();
				gameover_mc.visible = true;
				start_btn.visible = true;
				mc_gameover_winback.visible = true;
				highscore_txt.text = String(score);
			}
		}
		
		//*************************
		// Public methods:
		
		public function lives_amrk_init():void{
			mc_livemark1.visible = false;
			mc_livemark2.visible = false;
			mc_livemark3.visible = false;		
		}
		
		public function initiate():void
		{
			// Set defaults
			score = 0;
			level = 1;
			lives = 3;
			buttonFadeOut = true;
			gameover_mc.visible = false;
			start_btn.visible = false;
			mc_gameover_winback.visible = false;
			// Set players
			players.initiate();
		}
	}
}