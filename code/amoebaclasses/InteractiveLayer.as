﻿package code.amoebaclasses
{
	/*****************************************
	 * InteractiveLayer :
	 * This layer separates the player area from the 
	 * user interface so that players and obstacles can 
	 * be generated dynamically while appearing below
	 * the user interface. The InteractiveLayer contains
	 * the majority of the game functionality.
	 * -------------------
	 * See 2_amoebas.fla
	 ****************************************/

	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.utils.getTimer;

	import fl.transitions.Tween;
	import fl.transitions.easing.*;


	public class InteractiveLayer extends MovieClip
	{
		//*************************
		// Properties:

		public var radius2:Number = 0;
		public var centerx:Number = 0;
		public var centery:Number = 0;

		public var started:Boolean = true;

		//add by ljy
		public var cellNumber:Number = 1;
		public var totalCells:Number = 0;

		public var amoebacellNumber:Number = 1;
		public var amoebatotalCells:Number = 0;

		public var globcellNumber:Number = 1;
		public var globtotalCells:Number = 0;

		public var guidedcellNumber:Number = 1;
		public var guidedtotalCells:Number = 0;

		// Sounds
		public var backSnd:Sound;
		public var backSndChannel:SoundChannel;
		public var soundStartable:Boolean = true;
		public var cat1Snd:Sound,cat2Snd:Sound;
		public var cat1SndChannel:SoundChannel,cat2SndChannel:SoundChannel;

		// Assets
		public var sum_totalcell:Number = 0;

		//add by ljy
		public var cells:Object = new Object();
		public var amoebacells:Object = new Object();
		public var globcells:Object = new Object();
		public var guidedcells:Object = new Object();

		// Reference to game
		public var owner;
		public var amoebaCell;
		public var back1;
		public var back2;
		public var offset:Number = 0;
		public var H_back:Number = 1440;
		public var setpos:Number = offset - H_back;
		public var add_backspeed:Number = 0.3;
		public var wall_speed:Number = 3;

		//public var myTween:Tween;
		//public var ori_W:Number = 70;// original width of ship
		//public var ori_H:Number = 135.9;// original height of ship
		//public var target_W:Number = 1.3 * ori_W;
		//public var target_H:Number = 1.3 * ori_H;

		public var counter:Number = 0;
		//public var lives:Number = 3;

		public var cat1;
		public var cat2;
		public var cat3;
		public var cat4;
		public var step_score:Number = 0;
		public var total_time:Number = 0;
		public var now_time:Number = 0;
		public var temp_X:Number;
		public var temp_Y:Number;
		public var delta_X:Number;
		public var delta_Y:Number;
		public var level_flag:Boolean = true;
		public var calc_flag:Boolean = false;
		public var cat_speed:Number = 0;
		public var left:Boolean = false;
		public var left_cat_num:Number = 0;
		public var right_cat_num:Number = 0;
		public var appearances_num:Number = 0;
		public var appearances_score:Number = 0;
		public var temp_score:Number = 0;
		public var appearances_flag:Boolean = false;
		public var level_score:Number = 0;
		public var step_Y:Number = 0;
		public var last_score:Number = 0;

		public var img_v:Number = 0;
		//*************************
		// Constructor:

		public function InteractiveLayer()
		{
			// Wait to be started...
		}

		//*************************
		// Lifecycle...

		public function live( ref:* ):void
		{
			// Set game level reference
			back1 = new wall_back();
			back1.setSpeed(3);
			back1.setPosY(-850);
			back2 = new wall_back();
			back2.setSpeed(3);
			back2.setPosY(-2290);
			addChild(back1);
			addChild(back2);
			setChildIndex(back1, 0);
			setChildIndex(back2, 0);

			//myTween = new Tween(ship, "width", Elastic.easeOut, target_W, ori_W, 1, true);
			//myTween = new Tween(ship, "height", Elastic.easeOut, target_H, ori_H, 1, true);

			cat1 = new Cat1();
			cat1.live(this);
			addChild(cat1);

			cat2 = new Cat2();
			cat2.live(this);
			addChild(cat2);

			//cat3 = new Cat3();
			cat3 = new Cat1();
			cat3.live(this);
			cat3.scaleX *=  -1;//horizontal flip
			cat3.x = 890;
			addChild(cat3);

			//cat4 = new Cat4();
			cat4 = new Cat2();
			cat4.live(this);
			cat4.scaleX *=  -1;//horizontal
			cat4.x = 230;
			addChild(cat4);

			delta_Y = 500 - cat2.y;
			delta_Y = Math.abs(delta_Y);
			total_time = 0.2 * Math.sqrt(delta_Y);


			owner = ref;
			ship.live(this);

			backSnd = new backSound();
			backSndChannel = backSnd.play(0,50000);
			cat1Snd = new catsound_1();
			cat2Snd = new catsound_2();

			//backSndChannel.addEventListener( Event.SOUND_COMPLETE, onBackgroundMusicFinished );
			// Update screen every frame
			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}

		public function onBackgroundMusicFinished( event:Event ):void
		{
			backSndChannel = backSnd.play();
			backSndChannel.addEventListener( Event.SOUND_COMPLETE, onBackgroundMusicFinished );
		}

		public function die():void
		{
			removeEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}

		//*************************
		// Event Handling:

		protected function enterFrameHandler(event:Event):void
		{
			var msg_str:String = "";
			
			if (back1.y > 0 && img_v == 0)
			{
				back2.y = back1.y - H_back;
				img_v = 1;
			}
			if (back2.y > 0 && img_v == 1)
			{
				back1.y = back2.y - H_back;
				img_v = 0;
			}

			if (owner.score > owner.level * 1000)
			{
				owner.level++;

				msg_str = "Good Job! Level " + (owner.level - 1);
				msg_str = msg_str + "\nWow! Level " + owner.level + " Don't eat the spiders!";
				var msg_box:MessageBox = new MessageBox(msg_str);
				addChild(msg_box);
				msg_box.fadeIn = true;

				wall_speed = back1.getSpeed() + add_backspeed;
				back1.setSpeed(wall_speed);
				back2.setSpeed(wall_speed);
			}

			/*************************** cat motion start *******************************/

			//appearances_num = 2*owner.level;
			appearances_num = owner.level;
			appearances_score = Math.round(1000 / appearances_num);

			if ( (owner.score - last_score) > appearances_score )
			{
				appearances_num -=  1;
			}

			if (appearances_score + step_score < owner.score && appearances_num > 0 && calc_flag == false)
			{
				step_score +=  appearances_score;
				appearances_num -=  1;
				appearances_flag = true;
				last_score = owner.score;
			}

			if (appearances_flag)
			{
				left_cat_num = Math.round(Math.random() * 2);
				left_cat_num = left_cat_num % 2;
				right_cat_num = Math.round(Math.random() * 2);
				right_cat_num = right_cat_num % 2;

				if (ship.x < 435)
				{
					cat1SndChannel = cat1Snd.play();
					delta_X = ship.x - cat2.x;
					left = true;// for rigth cat
				}
				else
				{
					cat2SndChannel = cat2Snd.play();
					delta_X = ship.x - cat1.x;
					left = false;// for left cat
				}

				delta_X = Math.abs(delta_X);
				cat_speed = delta_X / total_time;

				calc_flag = true;
				appearances_flag = false;
			}

			if (calc_flag && now_time < total_time)
			{
				now_time +=  0.2;
				step_Y = 4.9 * Math.pow(now_time,2);
				if (left)
				{
					if (left_cat_num)
					{
						cat2.x -=  cat_speed / 5 + 10;
						cat2.y +=  step_Y;
						cat2.rotation -=  2;
						cat2.visible = true;
						cat3.visible = false;
					}
					else
					{
						cat3.x -=  cat_speed / 5 + 13;
						cat3.y +=  step_Y;
						cat3.rotation -=  2;
						cat3.visible = true;
						cat2.visible = false;
					}
				}
				else
				{
					if (right_cat_num)
					{
						cat1.x +=  cat_speed / 5 + 5;
						cat1.y +=  step_Y;
						cat1.rotation +=  2;
						cat1.visible = true;
						cat4.visible = false;
					}
					else
					{
						cat4.x +=  cat_speed / 5;
						cat4.y +=  step_Y;
						cat4.rotation +=  2;
						cat4.visible = true;
						cat1.visible = false;
					}
				}
			}
			else
			{
				if (left)
				{
					if (left_cat_num)
					{
						cat2.x = 650;
						cat2.y = 0;
						cat2.rotation = 0;
					}
					else
					{
						cat3.x = 890;
						cat3.y = 0;
						cat3.rotation = 0;
					}
				}
				else
				{
					if (right_cat_num)
					{
						cat1.x = 0;
						cat1.y = 0;
						cat1.rotation = 0;
					}
					else
					{
						cat4.x = 230;
						cat4.y = 0;
						cat4.rotation = 0;
					}
				}
				calc_flag = false;
				now_time = 0;
			}
			/*************************** cat motion end *******************************/
			if (ship.dead && owner.lives > 0)
			{
				ship.pauseTime = getTimer() - ship.deathTime;
				if (ship.pauseTime > 2000 && owner.lives > 0)
				{
					ship.dead = false;
					ship.fadeIn = true;
				}
			}

			if ( totalCells <= 3)
			{
				var id:String = "_" + cellNumber;
				var diff:Number = 100;
				diff +=  100;
				cells[id] = new Paramecium();
				cells[id].live(this,id, diff);

				addChild(cells[id]);
				setChildIndex(cells[id], 2);
				totalCells++;
				cellNumber++;
				//started = true;
			}

			//add by ljy
			if (amoebatotalCells <3)
			{

				var amoeba_id:String = "_" + cellNumber;
				amoebacells[amoeba_id] = new Amoeba();
				var diff1:Number = 100;
				diff1 +=  100;
				amoebacells[amoeba_id].live(this,amoeba_id, diff1);

				addChild(amoebacells[amoeba_id]);
				setChildIndex(amoebacells[amoeba_id], 2);
				amoebatotalCells++;
				cellNumber++;
				//started = true;
			}

			if (globtotalCells <= 2 && owner.level > 3)
			{

				var glob_id:String = "_" + cellNumber;
				globcells[glob_id] = new Glob();
				var diff2:Number = 100;
				diff2 +=  100;

				globcells[glob_id].live(this,glob_id, diff2);

				addChild(globcells[glob_id]);
				setChildIndex(globcells[glob_id], 2);
				globtotalCells++;
				cellNumber++;
				//started = true;
			}

			if (guidedtotalCells <= 3 && owner.level > 2)
			{

				var guided_id:String = "_" + cellNumber;
				guidedcells[guided_id] = new GuidedCell();
				var diff3:Number = 100;
				diff3 +=  100;

				guidedcells[guided_id].live(this,guided_id, diff3);

				addChild(guidedcells[guided_id]);
				setChildIndex(guidedcells[guided_id], 2);
				guidedtotalCells++;
				cellNumber++;
				//started = true;
			}
			sum_totalcell = totalCells + amoebatotalCells + globtotalCells + guidedtotalCells;
		}

		//*************************
		// Public methods:

		public function initiate():void
		{
			// Clean up
			for (var i in cells)
			{
				if (cells[i] != null)
				{
					removeChild(cells[i]);
				}
			}
			cells = new Object();
			// Reset values
			//cellNumber = 1;
			totalCells = 0;

			//add by ljy
			for (var j in amoebacells)
			{
				if (amoebacells[j] != null)
				{
					removeChild(amoebacells[j]);
				}
			}

			amoebacells = new Object();

			amoebatotalCells = 0;

			for (var k in globcells)
			{
				if (globcells[k] != null)
				{
					removeChild(globcells[k]);
				}
			}
			globcells = new Object();

			globtotalCells = 0;

			for (var l in guidedcells)
			{
				if (guidedcells[l] != null)
				{
					removeChild(guidedcells[l]);
				}
			}
			guidedcells = new Object();
			guidedtotalCells = 0;

			// Reset values
			cellNumber = 1;
			//totalCells = 0;

			// Reset ship
			ship.rotation = 0;
			ship.fadeIn = true;
			wall_speed = 3;
			back1.setSpeed(wall_speed);
			back2.setSpeed(wall_speed);
			step_score = 0;
		}

		public function wrapAround( whichObject ):void
		{
			var delta_x:Number = centerx - whichObject.x;
			var delta_y:Number = centery - whichObject.y;
			var angle:Number = Math.atan2(delta_y,delta_x);
			var distance:Number = Math.sqrt((delta_x*delta_x)+(delta_y*delta_y));
			if ( distance > radius2 * 2 )
			{
				whichObject.x = (Math.cos(angle)*(radius2))+centerx;
				//whichObject.y = (Math.sin(angle)*(radius2))+centery;
				whichObject.y = -60;
			}
		}

		public function collisions( whichObject ):void
		{
			// Collisions with ship
			if ( whichObject.hitTestPoint((ship.x),(ship.y),true) && ship.alpha>.9 )
			{
				whichObject.hit();
				//myTween = new Tween(ship, "width", Elastic.easeOut, ori_W, target_W, 1, true);
				//myTween = new Tween(ship, "height", Elastic.easeOut, ori_H, target_H, 1, true);
				if ( whichObject is Glob )
				{
					owner.score -=  150;
					counter +=  150;
					if (counter >= 750)
					{
						death(ship);
						counter = 0;
					}
					if (owner.score < 0)
					{
						owner.score = 0;
					}
				}
				else if ( whichObject is GuidedCell )
				{
					owner.score +=  75;

				}
				else if ( whichObject is Amoeba )
				{
					owner.score +=  50;
				}
				else if ( whichObject is Paramecium )
				{
					owner.score +=  25;
				}
				else if ( whichObject is Cat1 )
				{
					death(ship);
				}
				else if ( whichObject is Cat2 )
				{
					death(ship);
				}
				else if ( whichObject is Cat3 )
				{
					//owner.score -= 50;
					death(ship);
				}
				else if ( whichObject is Cat4 )
				{
					//owner.score -= 50;
					death(ship);
				}
			}
		}

		public function explosion( whichObject ):void
		{
			if (whichObject is Paramecium)
			{
				var e:Explosion = new Explosion();
				e.x = whichObject.x;
				e.y = whichObject.y;
				addChild(e);
			}
			else if (whichObject is Amoeba)
			{
				var f:Explosion_50 = new Explosion_50();
				f.x = whichObject.x;
				f.y = whichObject.y;
				addChild(f);
			}
			else if (whichObject is GuidedCell)
			{
				var g:Explosion_75 = new Explosion_75();
				g.x = whichObject.x;
				g.y = whichObject.y;
				addChild(g);
			}
			else if (whichObject is Glob)
			{
				var h:Explosion_150 = new Explosion_150();
				h.x = whichObject.x;
				h.y = whichObject.y;
				addChild(h);
			}
		}

		public function death( whichShip ):void
		{
			if (owner.lives != 0 && whichShip.alpha > .9)
			{
				explosion( whichShip );
			}

			if ( whichShip == ship )
			{
				ship.deathTime = getTimer();
				if (! ship.dead)
				{
					owner.lives--;
				}
				ship.clear();
				ship.dead = true;
				ship.alpha = 0;
				ship.x = 302;
				ship.y = 350;
			}
			else
			{
				whichShip.appearTime = getTimer();
				whichShip.pauseTime = (Math.random()*1000)+1000;
				whichShip.visible = false;
			}
		}

		public function removeCell(id:String):void
		{
			if (cells[id] != null)
			{
				totalCells--;
				removeChild(cells[id]);
				delete cells[id];
			}
			if (amoebacells[id] != null)
			{
				amoebatotalCells--;
				removeChild(amoebacells[id]);
				delete amoebacells[id];
			}
			if (globcells[id] != null)
			{
				globtotalCells--;
				removeChild(globcells[id]);
				delete globcells[id];
			}
			if (guidedcells[id] != null)
			{
				guidedtotalCells--;
				removeChild(guidedcells[id]);
				delete guidedcells[id];
			}
			//trace('id'+id);
			sum_totalcell--;
		}
	}
}