﻿package  code.amoebaclasses
{
    import flash.display.Sprite;
    import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.Event;
	import flash.utils.getTimer;
	
	public class MessageBox  extends Sprite{
    	
		public var msg_str:String;
		public var startTime:Number;
		public var endTime:Number;
		public var fadeIn:Boolean = false;
		
		var msgbox:Sprite = new Sprite();
		var textfield:TextField = new TextField();
        function MessageBox(str:String):void {
			x = 300;
			y = 250;

			// drawing a white rectangle
			msgbox.graphics.beginFill(0x000000, 0.7); // white
			msgbox.graphics.drawRect(0, 0, 300, 100); // x, y, width, height
			msgbox.graphics.endFill();
 
          // drawing a black border
			msgbox.graphics.lineStyle(2, 0x000000, 0.7);  // line thickness, line color (black), line alpha or opacity
			msgbox.graphics.drawRect(0, 0, 300, 100); // x, y, width, height
			
			var textF:TextFormat = new TextFormat();
			textF.color = 0xffffff;
			textF.font = "Times New Roman Bold"
			textF.size = 16;
			textF.align = "center";
			
			textfield.defaultTextFormat = textF;
			textfield.width = 260;
			textfield.height = 50;
			textfield.y = 20;
			textfield.x = 20;
			textfield.selectable = false;    			
			textfield.text = str;
			addChild(msgbox);
			addChild(textfield);
			alpha = 0;
			startTime = getTimer();
			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
        } 
 		protected function enterFrameHandler(event:Event):void
		{
			if( alpha < 1 && fadeIn ){
				alpha += .06;
			}else{
				fadeIn = false;
			}
			
			endTime = getTimer() - startTime;
			if(endTime > 2000 && fadeIn == false){
				alpha -= .1;
				if(alpha < 0.1)	textfield.text = "";
			}else{
				fadeIn = true;
			}
		}
     }
}
