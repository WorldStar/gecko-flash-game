﻿package  code.amoebaclasses
{
	import flash.events.Event;
	import flash.display.MovieClip;
	
	public class Cat1 extends MovieClip{
		public var owner;

		public function Cat1() {
			// constructor code
		}
		
		public function live( ref:*):void
		{
			// Set game level reference
			owner = ref;
			x = 0;
			y = 0;
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}

		
		public function die():void{
			removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		protected function enterFrameHandler(event:Event):void{
//			if(y < target_Y){
//				x -= time;
//				y -= 0.5*time*time;
//			}
			owner.collisions(this);
		}

		public function hit():void
		{
			
			owner.explosion(this);
//			owner.removeCell(id);
		}



	}
	
	
}
