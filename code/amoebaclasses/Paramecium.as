﻿package code.amoebaclasses
{
	/*****************************************
	 * Paramecium :
	 * Creates an obstacle object in the game.
	 * -------------------
	 * See 2_amoebas.fla
	 ****************************************/
	 
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.events.Event;
	import flash.utils.getTimer;
	import flash.text.TextField;
	import flash.display.MovieClip;
	
	public class Paramecium extends MovieClip
	{
		//*************************
		// Properties:
		
		public var moveTime:Number = 0;
		public var direction:Number = 0;
		public var speed:Number = 0;
		public var up:Boolean = false;
		public var down:Boolean = false;
		public var left:Boolean = false;
		public var right:Boolean = false;
		
		// Game index for 
		// this instance...
		public var id:String;
		public var owner;
		//Sound
		public var antSnd:Sound;
		public var antSndChannel:SoundChannel;
		public var append_rand:Number = 0;

		//*************************
		// Constructor:
		
		public function Paramecium()
		{
			// Wait to be started...
		}
		
		//*************************
		// Lifecycle...
		
		public function live( ref:*, i:String, diff:Number ):void
		{
			// Set game level reference
			owner = ref;
			id = i;
			
			// Set properties at start up
			append_rand = Math.random() * 600;
			x = Math.random() * (append_rand + diff) + 100;
			y = Math.random() * (append_rand - diff) - 200;
			
//			if(y > 450) 	y = 450;
//			if(y < 300) 	y = 300;
//
//			if(x > 660) 	x = 660;
//			if(x < 225) 	x = 225;
			
			moveTime = getTimer();
			speed = Math.random()*4 + 3;
			direction = Math.round(Math.random()*3);
			antSnd = new antSound();
			// Update screen every frame
			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}
		
		public function die():void
		{
			// do stop routine...
			removeEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}
		
		//*************************
		// Event Handling:
		
		protected function enterFrameHandler(event:Event):void
		{
			// Handle movement
			if( down && left ){
				go(-135);
			}else if( down && right ){
				go(135);
			}else if( down && !right && !left ){
				go(180);
			}
			// Calculate next move. modify by ljy
			if( direction == 0 ){
				up = false;
				down = true;
				right = false;
				left = false;
			}
			if( direction == 1 ){
				up = false;
				down = true;
				right = true;
				left = false;
			}
			if( direction == 2 ){
				up = false;
				down = true;
				right = false;
				left = true;
			}
				
			// Check for collisions
			owner.collisions(this);
			
			// Handle offscreen movement
			owner.wrapAround(this);
			 
			// Change vertical direction every 2-4 seconds
			if((getTimer()-moveTime)>(Math.random()*2000 + 1000))
			{
				direction = Math.round(Math.random()*3);
				moveTime = getTimer();
			}
			if(x > 580){
				direction = Math.round(Math.random()*3);
				if(direction == 1) direction = 2
			}
			if(x < 300){
				direction = Math.round(Math.random()*3);
				if(direction == 2) direction = 1
			}
		}
		
		//*************************
		// Public methods:
		
		public function hit():void
		{
//			x += 20;
			
			// If paramecium collides with any shot, 
			// it explodes and disappears
			
			antSndChannel = antSnd.play();
			owner.explosion(this);
			owner.removeCell(id);
		}
		
		public function go( degree ):void
		{
			y -= speed*Math.cos(rotation*(Math.PI/180));
			x += speed*Math.sin(rotation*(Math.PI/180));
			
			y += owner.wall_speed;
			
			if( rotation < degree ){
				rotation += 15;
			}
			if( rotation > degree ){
				rotation -= 15;
			}
		}
	}
}