﻿package code.amoebaclasses
{
	/*****************************************
	 * Amoeba :
	 * Creates an obstacle object in the game.
	 * -------------------
	 * See 2_amoebas.fla
	 ****************************************/

	import flash.events.Event;
	import flash.display.MovieClip;

	public class wall_back extends MovieClip{
		public var flow_flg;
		
		public var speed:Number;

		public function wall_back(){
			x = 210;
			flow_flg = false;
			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}
		public function die():void{
			removeEventListener(Event.ENTER_FRAME,enterFrameHandler);
		}
		protected function enterFrameHandler(event:Event):void{
			y +=speed;
		}
		
		public function setSpeed(speed_v:Number):void{
			speed = speed_v;
		}
		public function getSpeed():Number{
			return speed;
		}
		public function setPosY(pos_Y:Number):void{
			y = pos_Y;
		}
	}
}